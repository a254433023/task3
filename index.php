<?php
require_once 'backend/sdbh.php';
$dbh = new sdbh();
$products = $dbh->make_query('SELECT * FROM a25_products');
$services = unserialize($dbh->mselect_rows('a25_settings', ['set_key' => 'services'], 0, 1, 'id')[0]['set_value']);
$services_count = 0;

if( isset($_POST['submit']) ) {
    $res_product = json_decode($_POST['product'], true);
    $res_product_days = $res_product['days'];
    $res_product_price = $res_product['price'];
    $res_options = $_POST['options'];
    $res_days = $_POST['days'];

    if($res_product_days) {
        ksort($res_product_days);
        foreach($res_product_days as $key => $value) {
            if($key <= $res_days) {
                $res_product_price = $value;
            }
        }
    }

    $sum = $res_product_price * $res_days;

    if($res_options) {
        foreach($res_options as $value) {
            $sum += $value * $res_days;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <link href="assets/css/style.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"  crossorigin="anonymous"></script>
</head>
<body>

    <div class="container">

        <div class="row row-header">
            <div class="col-12">
                <img src="assets/img/logo.png" alt="logo" style="max-height:50px"/>
                <h1>Прокат</h1>
            </div>
        </div>

        <!-- TODO: реализовать форму расчета -->
        <div class="row row-form">
            <div class="col-12">
                
                <h4>Форма расчета:</h4>
                <?php if($sum): ?>
                    <div class="col-6 mt-4">
                        <div class="alert alert-primary" role="alert">Итоговая стоимость: <? echo number_format($sum, 0, null, ' '); ?> руб.</div>
                    </div>
                <? endif; ?>

                <div class="col-6">
                    <form method="POST" id="form">
                        <label class="form-label" for="product">Выберите продукт:</label>

                        <select class="form-select" name="product" id="product">  
                            <? foreach($products as $value): ?>
                                <?
                                    $product_merge = [
                                        'days' => unserialize($value['TARIFF']),
                                        'price' => $value['PRICE']
                                    ];
                                ?>
                                <option value=' <? echo json_encode($product_merge); ?> '><?=$value['NAME']?></option>
                            <? endforeach; ?>
                        </select>

                        <label for="days" class="form-label mt-2">Количество дней: <span id="days-output"></span></label>
                        <input type="range" class="form-range" id="days" name="days" min="1" max="30">

                        <p class="form-label mt-2">Дополнительно:</p>

                        <? foreach($services as $key => $value): ?>
                            <div class="form-check">
                                <input class="form-check-input" name="options[]" type="checkbox" value="<?=$value?>" id="flexCheckChecked<?=$services_count?>">
                                <label class="form-check-label" for="flexCheckChecked<?=$services_count?>"><?=$key?> за <?=$value?></label>
                            </div>
                            <? $services_count++; ?>
                        <? endforeach; ?>

                        <button type="submit" name="submit" class="btn btn-primary mt-4">Рассчитать</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <script src="assets/scripts/app.js" defer></script>
</body>
</html>