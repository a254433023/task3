let formDays = document.getElementById('days')
let formDaysOutput = document.getElementById('days-output')

formDaysOutput.innerHTML = formDays.value

formDays.addEventListener('input', () => { 
    formDaysOutput.innerHTML = formDays.value
});